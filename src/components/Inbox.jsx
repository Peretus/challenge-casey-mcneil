import { Row, Icon } from 'antd';
import { format } from 'date-fns';
import React from 'react';
import styled from 'styled-components';
import payload from '../payload';

const Loading = styled(Icon)`
  font-size: 40px;
  margin-top: 200px;
`;

const ListingImage = styled.img`
  height: 40px;
  width: auto;
`;

const InquiryStatus = ({ inquiry }) => {
  const { status } = inquiry;
  var prettyStatuses = {
    'pending_adjudication': 'Under Storetront Review',
    'paid': 'Booked and paid',
    'approved': 'In discussion',
    'default': ''
  };
  
  return prettyStatuses[status] || prettyStatuses.default;
}

const InquiryItem = ({ inquiry }) => {
  const {photo_small, title } = inquiry.listing;
  const {start_date, end_date } = inquiry.terms;
  const prettyStartDate = format(start_date, 'MMMM DD, YYYY');
  const prettyEndDate = format(end_date, 'MMMM DD, YYYY');
  const prettyDateRange = `${prettyStartDate} - ${prettyEndDate}`
  
  return (
    <div>
      <ListingImage src={photo_small} />
      <div>{title}</div>
      <div>{prettyDateRange}</div>
      <InquiryStatus inquiry={inquiry} />
    </div>
  );
}

const InquiriesHeader = () => {
  return (
    <div>
      <div>inquiries About Your Listings</div>
      <div>When prospective renters contact you about your listings, their inquiries are listed here.</div>
    </div>
  );
};

const InquiriesList = ({ inquiries }) => {
  return(
    <div>
      <InquiriesHeader />
      {inquiries.map(inquiry =>
        <InquiryItem key={inquiry.id} inquiry={inquiry}/>
      )}
    </div>
  );
}

class Inbox extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      inquiries: [],
    };
  }

  componentDidMount() {
    setTimeout(() => this.fetchInquiries(), 2000)
  }

  fetchInquiries() {
    this.setState({ loading: false, inquiries: payload.data });
  }

  render() {
    const { loading, inquiries } = this.state;

    return (
      <Row type="flex" justify="center">{
        loading ?
          <Loading type="loading" /> :
          <InquiriesList inquiries={inquiries} />}
      </Row>
    );
  }
}

export default Inbox;
