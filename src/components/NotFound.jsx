import React from 'react';

const NotFound = () =>
  <div>Whoops, looks like you've lost your way.</div>;

export default NotFound;
