import React from 'react';
import { render } from 'react-dom';
import 'antd/dist/antd.css';
import Router from './components/Router';

// eslint-disable-next-line react/jsx-filename-extension
render(<Router />, document.getElementById('main'));
